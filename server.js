const express = require("express");

// mongoose is a package that allows us to create Schemas(bluprint) our data structurtes and to manipulate our database using different access methods
const mongoose = require("mongoose");

const port = 3001;

const app = express();

//	[SECTION]
	//syntax: 
		//mongoose.connect("mongoDBconnectionString", {options to avoid errors in our connection})
	mongoose.connect("mongodb+srv://admin:admin@batch245-sayosa.g75wxqn.mongodb.net/s35-discussion?retryWrites=true&w=majority", 
	{
		//allows us to avoid any and
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


	let db = mongoose.connection;

	//error handling in connecting
	db.on("error", console.error.bind(console, "connetion error"));
	//this will
	db.once("open", ()=> console.log("We're connected to the cloud database!"))

	//mongoose schemas
		//Schemas determine the structure of the documents to be written in the database
		//Schemas act as the blueprint to our data
		//Syntax
			//const schemaName = new mongoose.Schema({keyvaluepairs})

	//task schema it contains two properties: name & status
	//require is used to specify that a field must not be empty
	//default - pending
	const userSchema = new mongoose.Schema({
		username: {
			type: String,
			required: [true, "Username is required "]
		},
		password: {
			type: String,
			default:"*********"
		}
	})

	//[SECTION]

		//uses schema to create/instatiate documents/ objects that follows our schema structure

		//the variable/object that will be created can be used to run commands with our database

		//model syntax
			//const variableName = mongoose.model("collectionName", schemaName);

	const User = mongoose.model("User", userSchema)

// middlewares
	app.use(express.json())// allows the app to read json data
	app.use(express.urlencoded({extended: true})); //it will allow our app to read data from forms.

	//[SECTION] Routing
		//Create/add new task
		//1.Check if the task is existing.
		//it task already exist in the database, we will return a message "the task is already existing"
		//it task doesnt exist in the database the database, we will add it in the database

app.post("/signup", (request, response)=>{
	let input = request.body;
	console.log(input.password);
	if(input.password === undefined){   User.findOne({username: request.body.username}, (error, result) =>{
        console.log(result);

        if(result !== null){
            return response.send("username is already exists!");
        }else{
            let newUser = new User({
                username: request.body.username
            })
            // save() method will save the object in the collection that the object instatiated
            newUser.save((saveError, savedUser) => {
                if (saveError){
                    return console.log(saveError)
                }else{
                    return response.send("New User created!")
                }
            })
        }
    })
}else{
	User.findOne({username: request.body.username}, (error, result) =>{
        console.log(result);

        if(result !== null){
            return response.send("The user is already existing!");
        }else{
            let newUser = new User({
                username: input.username,
                password: input.password
            })
            // save() method will save the object in the collection that the object instatiated
            newUser.save((saveError, savedUser) => {
                if (saveError){
                    return console.log(saveError)
                }else{
                    return response.send("New User created!")
                }
            })
        }
    })

}
 });



//[SECTION] Retrieving all the tasks
app.get("/users", (request, response) => {
	User.find({}, (error, result) =>{
		if(error){
			console.log(error);
		}else{
			return response.send(result);
		}
	})
})




app.listen(port, ()=> console.log(`Server is running at port ${port}`))




